class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

class SinglyLinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
    this.length = 0;
  }
  push(data) {
    let newNode = new Node(data);
    // if head exist
    if (this.head) {
      this.tail.next = newNode;
      this.tail = newNode;
      // if head is not exist
    } else {
      this.head = newNode;
      this.tail = this.head;
    }
    this.length++;
    return this;
  }
  pop() {
    if (!this.head) return undefined;
    let current = this.head;
    let newTail = current;
    while (current.next) {
      newTail = current;
      current = current.next;
    }
    newTail.next = null;
    this.tail = newTail;
    this.length--;
    if (this.length === 0) {
      this.head = null;
      this.tail = null;
    }
    return current;
  }
  shift() {
    if (!this.head) return undefined;
    let preHead = this.head;
    if (this.head.next) {
      this.head = preHead.next;
    } else {
      this.head = null;
      this.tail = null;
    }
    this.length--;
    return preHead;
  }
  unshift(data) {
    let newHead = new Node(data);
    if (this.head) {
      const preHead = this.head;
      newHead.next = preHead;
      this.head = newHead;
    } else {
      this.head = newHead;
      this.tail = newHead;
    }
    this.length++;
    return this;
  }
  get(index) {
    if (index < 0 || index >= this.length) return null;
    let current = this.head;
    let counter = 0;
    while (current) {
      if (index === counter) {
        return current;
      }
      current = current.next;
      counter++;
    }
  }
  set(index, data) {
    let node = this.get(index);
    if (!node) return false;
    node.data = data;
    return true;
  }
  insert(index, data) {
    let newNode = new Node(data);
    let current = this.head;
    let counter = 0;
    while (current) {
      if (counter === index - 1) {
        current.next = newNode;
      } else if (counter === index) {
        newNode.next = this.get(index);
        return this;
      }
      counter++;
      current = current.next;
    }
  }
  remove(index) {
    if (index < 0 || index >= this.length) return undefined;
    if (index === 0) return this.shift();
    if (index === this.length - 1) return this.pop();
    var previousNode = this.get(index - 1);
    var removed = previousNode.next;
    previousNode.next = removed.next;
    this.length--;
    return removed;
  }
}

class Contact {
  constructor(name, phoneNumber) {
    this.name = name;
    this.phoneNumber = phoneNumber;
  }
}

class PhoneBook {
  constructor() {
    this.contacts = new SinglyLinkedList();
  }

  create(name, phoneNumber) {
    const newContact = new Contact(name, phoneNumber);
    this.contacts.push(newContact);
  }

  getAll(callback) {
    if (this.contacts.head) {
      let current = this.contacts.head;
      let index = 0;
      while (current) {
        callback(index, current);
        index++;
        current = current.next;
      }
    }
  }

  getByIndex(index) {
    return this.contacts.get(index);
  }

  update(index, obj) {
    let oldNodeData = this.contacts.get(index).data;
    Object.assign(oldNodeData, obj);
  }

  detele(index) {
    this.contacts.remove(index);
  }
}

let phoneBook = new PhoneBook();

phoneBook.create("Rod M Lewis", "605-779-2397");
phoneBook.create("Nicholas D Fields", "713-677-0818");
phoneBook.create("Heather G Ortega", "713-677-0818");

const createForm = document.getElementById("createForm");
const form = document.querySelector(".form");
const updateForm = document.getElementById("updateForm");
const contactList = document.getElementById("contactList");

const addToList = (index, obj) => {
  let li = `
  <li
  id="${index}"
  class="list-group-item d-flex align-items-center justify-content-between"
>
  <span class="d-flex align-items-center">
    <img
      src="./img/avatar.png"
      style="width: 50px;"
      class="mr-2"
    />
    <span>
      <span class="name h5">${obj.data.name}</span>
      <span class="phone">${obj.data.phoneNumber}</span>
    </span>
  </span>
  <span>
    <button id="${index}" href="#" class="badge btn btn-primary">update</button>
    <a id="${index}" href="#" class="badge btn btn-danger">delete</a>
  </span>
</li>`;
  contactList.innerHTML += li;
};

const refreshList = () => {
  contactList.innerHTML = "";
  phoneBook.getAll(addToList);
};

refreshList();

createForm.addEventListener("submit", e => {
  e.preventDefault();
  if (createForm.name.value && createForm.phone.value) {
    phoneBook.create(createForm.name.value.trim(), createForm.phone.value);
    refreshList();
    createForm.reset();
  }
});

contactList.addEventListener("click", e => {
  e.preventDefault();
  if (e.target.innerText === "update") {
    const index = Number(e.target.getAttribute("id"));
    const contactData = phoneBook.getByIndex(index).data;
    updateForm.name.value = contactData.name;
    updateForm.phone.value = contactData.phoneNumber;
    updateForm.index.value = index;
  }
});

contactList.addEventListener("click", e => {
  e.preventDefault();
  if (e.target.innerText === "delete") {
    const index = Number(e.target.getAttribute("id"));
    phoneBook.detele(index);
    refreshList();
  }
});

updateForm.addEventListener("submit", e => {
  e.preventDefault();
  const updateObj = {
    name: updateForm.name.value.trim(),
    phoneNumber: updateForm.phone.value
  };
  phoneBook.update(Number(updateForm.index.value), updateObj);
  refreshList();
  updateForm.reset();
});
